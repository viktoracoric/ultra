# Ultra

### Fork of Ultra minimal launcher app for Android (24kB)

This fork removes auto-open feature of the original when only one search result remained and adds two more home app slots.

### Download this version via [APK release](https://github.com/viktoracoric/Ultra/releases/)

### Download original from [Play Store](https://play.google.com/store/apps/details?id=app.olauncher.ultra) or [APK release](https://github.com/tanujnotes/Ultra/releases/)
